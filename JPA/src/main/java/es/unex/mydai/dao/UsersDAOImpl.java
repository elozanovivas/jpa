package es.unex.mydai.dao;

import javax.persistence.EntityManager;

import es.unex.mydai.persistence.vo.Users;

public class UsersDAOImpl extends GenericDAOImpl<Users, Long> implements UsersDAO {

	public UsersDAOImpl(Class<Users> entityClass, EntityManager entityManager) {
		super(entityClass, entityManager);
	}
}
