package es.unex.mydai.dao;

import es.unex.mydai.persistence.vo.Users;

public interface UsersDAO extends GenericDAO<Users, Long> {
}
