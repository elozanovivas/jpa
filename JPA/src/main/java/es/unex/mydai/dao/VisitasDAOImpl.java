package es.unex.mydai.dao;

import javax.persistence.EntityManager;

import es.unex.mydai.persistence.vo.Visitas;

public class VisitasDAOImpl extends GenericDAOImpl<Visitas, Long> implements VisitasDAO {

	public VisitasDAOImpl(Class<Visitas> entityClass, EntityManager entityManager) {
		super(entityClass, entityManager);
	}
}
