package es.unex.mydai.dao;

import es.unex.mydai.persistence.vo.Visitas;

public interface VisitasDAO extends GenericDAO<Visitas, Long> {
}
