package es.unex.mydai.dao;

import es.unex.mydai.persistence.vo.Hashtags;

public interface HashtagDAO extends GenericDAO<Hashtags, Long> {
}
