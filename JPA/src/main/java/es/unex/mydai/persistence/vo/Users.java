package es.unex.mydai.persistence.vo;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table (name= "USERS")
public class Users implements Serializable{


	
	@Id
	@GeneratedValue(generator = "increment")
	@GenericGenerator(name = "increment", strategy = "increment")
	private long idUsers;
	
	@Column(name="name_user")
	private String nameUser;
	
	@Column(name="pass")
	private String pass;
	
	@Column(name="email")
	private String email;
	
	@OneToMany
	@JoinColumn(name="idUsusarioorigen")
	private List<Mensajes> mensajesList;

	
	
	public Users() {
		super();
	}

	public Users(long idUsers, String nameUser, String pass, String email,
			List<Mensajes> mensajesList) {
		super();
		this.idUsers = idUsers;
		this.nameUser = nameUser;
		this.pass = pass;
		this.email = email;
		//this.mensajesList = mensajesList;
	}


	public long getIdUsers() {
		return idUsers;
	}

	public void setIdUsers(long idUsers) {
		this.idUsers = idUsers;
	}

	public String getNameUser() {
		return nameUser;
	}

	public void setNameUser(String nameUser) {
		this.nameUser = nameUser;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<Mensajes> getMensajesList() {
		return mensajesList;
	}

	public void setMensajesList(List<Mensajes> mensajesList) {
		this.mensajesList = mensajesList;
	}

	@Override
	public String toString() {
		return "Users [idUsers=" + idUsers + ", nameUser=" + nameUser
				+ ", pass=" + pass + ", email=" + email + ", mensajesList="
				/*+ mensajesList*/ + "]";
	}
	
	
	
}
