package es.unex.mydai.persistence.vo;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;


import es.unex.mydai.dao.HashtagDAO;
import es.unex.mydai.dao.HashtagDAOImpl;
import es.unex.mydai.dao.MensajesDAO;
import es.unex.mydai.dao.MensajesDAOImpl;
import es.unex.mydai.dao.UsersDAO;
import es.unex.mydai.dao.UsersDAOImpl;

public class Prueba {

	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("mydai");
		EntityManager em = emf.createEntityManager();

		// Open Connection
		em.getTransaction().begin();
		
		Users userTest = new Users();
		userTest.setNameUser("Emilio");
		userTest.setEmail("emilio@mcw.com");
		userTest.setPass("123");
		UsersDAO usersDAO = new UsersDAOImpl(Users.class, em);
		usersDAO.create(userTest);
		
		userTest.setNameUser("Emilio");
		userTest.setEmail("Emilio@mcw.com");
		userTest.setPass("Contrasena");
		usersDAO.create(userTest);
		
		
		Mensajes mensajePrueba=new Mensajes();
		mensajePrueba.setTexto("Holaaa!!!");
		mensajePrueba.setIdUsuarioOrigen(userTest);
		mensajePrueba.setIdUsuarioDestino(userTest);
		mensajePrueba.setPublico(true);
		
		Hashtags hashtagTest= new Hashtags();
		hashtagTest.setUser(userTest);
		hashtagTest.setHashtag("#Funciona");
		hashtagTest.setContenido("JPA");
		
		
		UsersDAO usersDAOa = new UsersDAOImpl(Users.class, em);
		usersDAOa.create(userTest);
		
		MensajesDAO mensajesDAO = new MensajesDAOImpl(Mensajes.class, em);
		mensajesDAO.create(mensajePrueba);
		
		HashtagDAO hashtagDAO = new HashtagDAOImpl(Hashtags.class, em);
		hashtagDAO.create(hashtagTest);
		
		
		em.getTransaction().commit();
		
		
		// Close
		em.close();
		emf.close();

	}

}
