package es.unex.mydai.persistence.vo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="VISITAS")

public class Visitas implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(generator = "increment")
	@GenericGenerator(name = "increment", strategy = "increment")
	private long idVisita;
	
	@Column (name="id_usuario_visitado")
	private long idUsuarioVisitado;
	
	@Column (name="id_usuario_visitante")
	private long idUsuarioVisitante;
	
	
	public Visitas() {
		super();
	}

	public Visitas(long idVisita, long idUsuarioVisitado,
			long idUsuarioVisitante) {
		super();
		this.idVisita = idVisita;
		this.idUsuarioVisitado = idUsuarioVisitado;
		this.idUsuarioVisitante = idUsuarioVisitante;
	}

	public long getIdVisita() {
		return idVisita;
	}

	public void setIdVisita(long idVisita) {
		this.idVisita = idVisita;
	}

	public long getIdUsuarioVisitado() {
		return idUsuarioVisitado;
	}

	public void setIdUsuarioVisitado(long idUsuarioVisitado) {
		this.idUsuarioVisitado = idUsuarioVisitado;
	}

	public long getIdUsuarioVisitante() {
		return idUsuarioVisitante;
	}

	public void setIdUsuarioVisitante(long idUsuarioVisitante) {
		this.idUsuarioVisitante = idUsuarioVisitante;
	}

	@Override
	public String toString() {
		return "Visitas [idVisita=" + idVisita + ", idUsuarioVisitado="
				+ idUsuarioVisitado + ", idUsuarioVisitante="
				+ idUsuarioVisitante + "]";
	}
	
	
	
}
