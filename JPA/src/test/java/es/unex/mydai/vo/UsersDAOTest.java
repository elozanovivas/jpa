package es.unex.mydai.vo;

import static org.junit.Assert.assertNotNull;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import es.unex.mydai.dao.HashtagDAO;
import es.unex.mydai.dao.HashtagDAOImpl;
import es.unex.mydai.dao.MensajesDAO;
import es.unex.mydai.dao.MensajesDAOImpl;
import es.unex.mydai.dao.UsersDAO;
import es.unex.mydai.dao.UsersDAOImpl;
import es.unex.mydai.persistence.vo.Hashtags;
import es.unex.mydai.persistence.vo.Mensajes;
import es.unex.mydai.persistence.vo.Users;

public class UsersDAOTest {

	private static EntityManagerFactory emf;

	private static EntityManager em;

	@BeforeClass
	public static void createEntityManagerFactory() {
		emf = Persistence.createEntityManagerFactory("mydai");
	}

	@AfterClass
	public static void closeEntityManagerFactory() {
		emf.close();
	}

	@Before
	public void beginTransaction() {
		em = emf.createEntityManager();
		em.getTransaction().begin();
	}

	@After
	public void rollBackTransaction() {
		if (em.getTransaction().isActive()) {
			em.getTransaction().rollback();
		} else {
			em.close();
		}
	}
	
	@Test
	public void addUserTestOK(){
		Users userTest = new Users();
		userTest.setNameUser("Emilio");
		userTest.setEmail("emilio@mcw.com");
		userTest.setPass("123");
		
		UsersDAO usersDAO = new UsersDAOImpl(Users.class, em);
		Users userResult =usersDAO.create(userTest);
		
		Users userTest2 = new Users();
		userTest2.setNameUser("Pepe");
		userTest2.setEmail("Pepe@mcw.com");
		userTest2.setPass("578");
		Users userResult2 =usersDAO.create(userTest2);
		
		Hashtags hashtag=new Hashtags();
		hashtag.setHashtag("#mydai");
		hashtag.setContenido("Metodologia y desarrollo de aplicaciones para Internet");
		hashtag.setUser(userTest);
		HashtagDAO hashtagDAO=new HashtagDAOImpl(Hashtags.class, em);
		Hashtags hashtagResult = hashtagDAO.create(hashtag);
		
		
		Mensajes mensaje=new Mensajes();
		mensaje.setTexto("Holaaa!!!");
		mensaje.setIdUsuarioOrigen(userTest);
		mensaje.setIdUsuarioDestino(userTest2);
		mensaje.setPublico(true);
		MensajesDAO mensajesDAO= new MensajesDAOImpl(Mensajes.class, em);
		Mensajes mensajeResult = mensajesDAO.create(mensaje);
		
		assertNotNull(userResult);
		assertNotNull(userResult.getIdUsers());

		assertNotNull(userResult2);
		assertNotNull(userResult2.getIdUsers());
		
		assertNotNull(hashtagResult);
		assertNotNull(hashtagResult.getIdHashtag());
		
		assertNotNull(mensajeResult);
		assertNotNull(mensajeResult.getIdMensaje());
		
		System.out.println("USER: " + userResult);
		System.out.println("USER2: " + userResult2);
		System.out.println("HASHTAG: "+ hashtagResult);
		System.out.println("MENSAJE: "+ mensajeResult);
		
	}
	
	
	
	
	
}
